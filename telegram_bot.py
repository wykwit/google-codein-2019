# Google Code-in Task: a simple bot which fetches the number of forks from the repo of Fedora-Infra

import requests
import json
import telegram.ext as tg

tg_api_key = "1016524471:AAG_rFryXonNs0I7JbcPKa9MrbzCHm7lYuY"
git_api_url = "https://api.github.com/orgs/fedora-infra/repos"

# git api handling
def get_data():
	with requests.get(git_api_url) as r:
		data = json.loads(r.text)
	return data

def get_forks_count():
	output_message = ""
	for x in get_data():
		output_message += x["name"] + ": " + str(x["forks_count"]) + "\n"
	return output_message

def get_repo_details(name):
	for x in get_data():
		if name in x["name"]:
			if "homepage" not in x:
				x["homepage"] = "None"
			return "Name: {}\nDescription: {}\nLanguage: {}\nHomepage: {}\nLast update: {}\nLicense: {}\nOpen issues: {}\nForks: {}"\
			.format(x["name"], x["description"], x["language"], x["homepage"], x["updated_at"], x["license"]["name"], x["open_issues_count"], x["forks_count"])
	return "Repository not found."

def get_repo_forks(name):
	for x in get_data():
		if name in x["name"]:
			return x["forks"]
	return "Repository not found."

# telegram handling
updater = tg.Updater(tg_api_key)

def cmd_start(bot, update):
	update.message.reply_text("Try out other commands:\n/update\n/details [name]\n/forks [name]\n")

def cmd_update(bot, update):
	update.message.reply_text(get_forks_count())

def cmd_details(bot, update, args):
	if len(args) and isinstance(args[0], str):
		update.message.reply_text(get_repo_details(args[0]))
	else:
		update.message.reply_text("You forgot the argument. Which repo do you want to fetch the details for?")

def cmd_forks(bot, update, args):
	if len(args) and isinstance(args[0], str):
		update.message.reply_text(get_repo_forks(args[0]))
	else:
		update.message.reply_text("You forgot the argument. Which repo do you want to fetch the forks for?")

updater.dispatcher.add_handler(tg.CommandHandler("start", cmd_start))
updater.dispatcher.add_handler(tg.CommandHandler("update", cmd_update))
updater.dispatcher.add_handler(tg.CommandHandler("details", cmd_details, pass_args=True))
updater.dispatcher.add_handler(tg.CommandHandler("forks", cmd_forks, pass_args=True))
updater.start_polling()
