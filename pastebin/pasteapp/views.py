from django.shortcuts import render

from .models import Paste

def display_home(request):
    pastes = Paste.objects.order_by('-date', '-id')
    return render(request, 'browse.html', {'pastes': pastes})

def display_paste(request, paste_id):
    pastes = Paste.objects.filter(id=paste_id)
    return render(request, 'paste.html', {'pastes': pastes})

def display_create(request):
	return render(request, 'create.html')

def submit_paste(request):
    if request.method == "POST" and "data" in request.POST and request.POST["data"]:
        if "title" in request.POST and request.POST["title"]:
            pastes = Paste(content=request.POST["data"], title=request.POST["title"])
        else:
            pastes = Paste(content=request.POST["data"])
        pastes.save()
        return display_paste(request, pastes.id)
    else:
        return display_home(request)
