from django.contrib import admin

from .models import Paste

class PasteAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'date', 'id')

admin.site.register(Paste, PasteAdmin)
