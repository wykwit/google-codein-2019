from django.db import models

from datetime import datetime

class Paste(models.Model):
    title = models.CharField(max_length=64, default="unnamed paste")
    content = models.TextField(default="")
    date = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.title
