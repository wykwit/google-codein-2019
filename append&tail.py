# Google Code-in Task: create a script that encodes and decodes a text from an image (png and jpg).

fname = input("Filename: ")
choice = input("[d]ecode or [e]ncode? ")
if choice == "e":
	data = input("Your data: ")
	data = "\n" + data.strip()
	with open(fname, "a") as f:
		f.write(data)
elif choice == "d":
	with open(fname, "rb") as f:
		data = f.readlines()[-1]
	print(data)
else:
	exit("invalid choice")
