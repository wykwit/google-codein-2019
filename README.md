I actively participated in [Google Code-in 2019](https://codein.withgoogle.com/) starting from 2nd December up until December 22nd.
The competition was still running for another month (the work submission deadline was 23rd January), but at that time I wasn't active anymore.

During the contest I've completed a few _more enjoyable_ tasks from Fedora Project. This repository contains some of my solutions.
