# Google Code-in Task: create a script that does both hashing and cracking

import argparse
import hashlib

hash_functions = ["md5", "sha1", "sha224", "sha256", "sha384", "sha512"]

parser = argparse.ArgumentParser(description="Hash or crack strings.")
parser.add_argument("-f", choices=hash_functions, required=True,\
	help="The hash function that will be used.")
parser.add_argument("--crack", dest="crack", action="store_true",\
	help="When present the string will be treated as a hash and the program will attempt to crack it.")
parser.add_argument("-i", dest="input_file",\
	help="This file will be hashed. In the cracking mode it will be read and treated as an input string.")
parser.add_argument("-P", dest="passwords",\
	help="A dictionary of passwords used to crack hashes. Passwords should be separated by newlines.")
args = parser.parse_args()

if args.input_file:
	with open(args.input_file, "r") as f:
		input_value = f.read()
else:
	input_value = input("Input string: ").strip()

def h(password, func):
	t = bytes(password, encoding="utf-8")
	r = ""
	if func == "md5":
		r = hashlib.md5(t)
	elif func == "sha1":
		r = hashlib.sha1(t)
	elif func == "sha224":
		r = hashlib.sha224(t)
	elif func == "sha256":
		r = hashlib.sha256(t)
	elif func == "sha384":
		r = hashlib.sha384(t)
	elif func == "sha512":
		r = hashlib.sha512(t)
	return r.hexdigest()

hashed = ""
if args.crack:
	input_value = input_value.strip()
	if args.passwords:
		with open(args.passwords, "r") as f:
			for password in f.readlines():
				password = password.strip()
				hashed = h(password, args.f)
				if hashed == input_value: break
	else:
		while hashed != input_value:
			password = input("Password to try: ").strip()
			if not password: break
			hashed = h(password, args.f)
			print("Hash of your input:", hashed)
	if hashed == input_value:
		print("Found your hash!")
		print(input_value, ":", password)
	else:
		print("Couldn't crack your hash :(")
else:
	print("The", args.f , "hash is:")
	print(h(input_value, args.f))
