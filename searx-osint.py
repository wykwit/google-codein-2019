# Google Code-in Task: script that checks out the presence of an individual in 3 different website

import requests
import json

base_url = "https://searx.me/"
post_data = {"category_general": 1, "time_range": "None", "format": "json"}

query = input("Query: ")
post_data["q"] = query

try:
	with requests.post(base_url, data=post_data) as r:
		data = json.loads(r.text)
except:
	print("something went wrong while fetching results")
	print("maybe you've exceeded your query limit for this domain")
	print("try again later")
	exit()

print("This script is using searx.me - the best search engine in existence.")

domains = []
for x in data["results"]:
	t = x["parsed_url"][1]
	if t not in domains:
		domains.append(t)
domains = sorted(domains)

print("Your query was related to following websites:\n")
for x in domains:
	print(x)
