import endpoints
import logic

print("This script burns Fedora ISO images on a drive of your choice.")
print("What kind of fedora variant are you interested in?")

for i, variant in enumerate(endpoints.variants):
	print(i+1, "-", variant)

variant = input("#> ")
if not variant:
	variant = 0
else:
	try:
		variant = int(variant.strip())-1
		assert variant < len(endpoints.variants)
	except:
		exit("invalid choice")
	if variant < 0: variant = 0

if variant == 0:
	download = endpoints.workstation_download
	checksum = endpoints.workstation_checksum
if variant == 1:
	print("Choose a Fedora Spin flavour:")
	for i, flavour in enumerate(endpoints.spins):
		print(i+1, "-", flavour)
	flavour = input("#> ")
	try:
		flavour = int(flavour.strip())-1
		assert flavour >= 0
		assert flavour < len(endpoints.spins)
	except:
		exit("invalid choice")
	download = endpoints.spins_download[endpoints.spins[flavour]]
	checksum = endpoints.spins_checksum
elif variant == 2:
	print("Choose a Fedora Lab flavour:")
	for i, flavour in enumerate(endpoints.labs):
		print(i+1, "-", flavour)
	flavour = input("#> ")
	try:
		flavour = int(flavour.strip())-1
		assert flavour >= 0
		assert flavour < len(endpoints.spins)
	except:
		exit("invalid choice")
	download = endpoints.labs_download[endpoints.labs[flavour]]
	checksum = endpoints.labs_checksum

print("Do you want to download the ISO now?")
choice = input("(y/n)> ").lower()
if choice == "y" or choice == "yes":
	iso = logic.download(download)
else:
	print("If you already have it downloaded, please specify the path to your ISO:")
	iso = input("''> ").strip()
	if not logic.file_exists(iso):
		exit("file doesn't exist")

print("Downloading Fedora's GPG keys...")
keyfile = logic.download(endpoints.gpg_download)
print("Importing Fedora's GPG keys...")
logic.gpg_import(keyfile)
print("Deleting GPG key file...")
logic.file_delete(keyfile)
print("Downloading a CHECKSUM file...")
checksum_file = logic.download(checksum)

print("Verifying a CHECKSUM file...")
if not logic.gpg_verify(checksum_file):
	print("Couldn't verify the CHECKSUM file, it might have been tampered with.")
	print("Are you sure you want to continue?")
	choice = input("(y/n)> ").lower()
	if choice != "y" and choice != "yes":
		exit("invalid CHECKSUM file")
else:
	print("Success.")

print("Verifying iso files...")
if not logic.hash_verify(iso, checksum_file):
	print("Checks failed, the files might be corrupted.")
	print("Are you sure you want to continue?")
	choice = input("(y/n)> ").lower()
	if choice != "y" and choice != "yes":
		exit("corrupted files")
else:
	print("Success.")
print("Deleting CHECKSUM file...")
logic.file_delete(checksum_file)

print("Choose your removable drive:")
drives = logic.removable_devices()
for i, drive in enumerate(drives):
	print(i+1, "-", drive, "("+logic.device_vendor(drive)+")")
print(len(drives)+1, "- custom")
drive = input("#> ")
try:
	drive = int(drive.strip())-1
	assert drive < len(drives)+1
except:
	exit("invalid choice")
if drive == len(drives) or drive < 0:
	print("Please specify the path to your drive:")
	drive = input("''> ").strip()
else:
	drive = drives[drive]
print("You chose `"+ drive+"`. Vendor:", logic.device_vendor(drive))
print("Data on that device will be overwritten.")
print("Are you sure you want to continue?")
choice = input("(y/n)> ").lower()
if choice != "y" and choice != "yes":
	exit("aborted")
print("Burning ISO...")
proc = logic.burn(iso, drive)
if proc.returncode == 0:
	print("Success.")
else:
	print("dd process returned", proc.returncode)
print("My job here is done. Enjoy your bootable drive!")

