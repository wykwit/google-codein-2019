#!/usr/bin/env python3

import argparse
import math
import sys
from base64 import b64encode, b64decode
from PIL import Image

def arguments():
	parser = argparse.ArgumentParser(description="Perform LSB steganography.")
	parser.add_argument("-d", dest="decrypt", action="store_true", help="Decrypt instead of encrypting.")
	parser.add_argument("-i", dest="input_file", required=True)
	parser.add_argument("-o", dest="output_file")
	parser.add_argument("-m", dest="message", help="Message that will be encrypted.")
	parser.add_argument("-b", dest="bits", default="1", help="Amount of bits on each pixel used for decryption.")
	return parser.parse_args()

def decrypt(bits, image):
	out = []
	to_read = -1
	for x in range(image.size[0]):
		for y in range(image.size[1]):
			r, g, b = image.getpixel((x, y))
			out.append(r&((1<<bits)-1))
			out.append(g&((1<<bits)-1))
			out.append(b&((1<<bits)-1))
			if to_read == -1 and len(out)*bits >= 32:
				to_read = int("".join([bin((1<<bits)+t)[3:] for t in out])[:32], 2)+32
			elif to_read != -1 and len(out)*bits > to_read: break
		if to_read != -1 and len(out)*bits > to_read: break
	tmp = "".join([bin((1<<bits)+x)[3:] for x in out])[32:to_read]
	out = []
	for i in range(0, len(tmp), 8):
		out.append(int(tmp[i:i+8], 2))
	return bytes(out)

def encrypt(data, image, output):
	tmp = "".join([bin(256+x)[3:] for x in data])
	bits_in_image = image.size[0] * image.size[1] * 3
	bits = math.ceil((len(tmp)+32)/bits_in_image)
	tmp = bin(len(tmp)+(1<<33-1))[3:]+tmp
	data = []
	for i in range(0, len(tmp), bits):
		data.append(int(tmp[i:i+bits], 2))
	x, y, i = 0, 0, 0
	while i < len(data):
		if x >= image.size[0]:
			break
		if y >= image.size[1]:
			x += 1
			y = 0
		r, g, b = image.getpixel((x, y))
		if i < len(data):
			r = (r>>bits<<bits) + data[i]
			i += 1
		if i < len(data):
			g = (g>>bits<<bits) + data[i]
			i += 1
		if i < len(data):
			b = (b>>bits<<bits) + data[i]
			i += 1
		image.putpixel((x, y), (r, g, b))
		y += 1
	image.save(output)
	return bits

def main(args):
	image = Image.open(args.input_file)
	if args.decrypt:
		data = decrypt(int(args.bits), image)
		print("base64:", data)
		data = b64decode(data)
		print("bytes:", data)
		if args.output_file:
			with open(args.output_file, "wb") as f:
				f.write(data)
		data = str(data, encoding="utf-8")
		print("UTF-8:", data)
	else:
		if not args.output_file:
			print("No output file specified. Where should I save the image?")
			args.output_file = input("Filename: ").strip()
		if not args.message:
			print("No message specified. Reading from stdin...")
			args.message = sys.stdin.read()
		data = b64encode(bytes(args.message, encoding="utf-8"))
		bits = encrypt(data, image, args.output_file)
		print("bits:", bits)
	image.close()

if __name__ == "__main__":
	args = arguments()
	main(args)

