#!/usr/bin/env python3

import gooey
import stegano

@gooey.Gooey
def arguments():
	parser = gooey.GooeyParser(description="Perform LSB steganography.")
	parser.add_argument("-i", "--input", dest="input_file", required=True, widget="FileChooser")
	parser.add_argument("-o", "--output", dest="output_file", widget="FileSaver")
	parser.add_argument("-d", "--decrypt", dest="decrypt", action="store_true", help="Decrypt instead of encrypting.", widget="BlockCheckbox")
	parser.add_argument("-b", dest="bits", default="1", help="Amount of bits per pixel used for decryption.")
	parser.add_argument("-m", "--msg", dest="message", help="Message that will be encrypted.")
	return parser.parse_args()

if __name__ == "__main__":
	args = arguments()
	stegano.main(args)

