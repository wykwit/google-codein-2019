#!/usr/bin/env python3

import json
import requests

api_url = "https://api.myip.com/"

with requests.get(api_url) as r:
	data = json.loads(r.text)

print("Your IP is:", data["ip"])
