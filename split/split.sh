#!/bin/bash

delimiters=$1
if [ "$delimiters" == "" ]; then
	read -p "Delimiters: " delimiters
	echo "Reading input from stdin..."
fi
prefix="xx"
id=0
str=""

while read -rN1 c
do
	if [[ "$delimiters" =~ "$c" ]]
	then
		echo "$str" >> "$prefix$id"
		id=$(( $id + 1 ))
		str=""
	else
		str="$str$c"
	fi
done
if [ "$str" != "" ]
then
	echo "$str" >> "$prefix$id"
fi