#!/bin/bash

# Google Code-in Task: script that checks out the presence of an individual in 3 different website

read -p "Nickname: " nickname
if $(curl -s "https://www.instagram.com/$nickname/" | grep -q "Sorry, this page isn"); then
	echo "not found on instagram"
else
	echo "found something on instagram.com"
fi
if $(curl -s "https://github.com/$nickname/" | grep -q "Not found"); then
	echo "not found on github"
else
	echo "found something on github.com"
fi
if $(curl -s "https://twitter.com/$nickname/" | grep -q "that page doesn"); then
	echo "not found on twitter"
else
	echo "found something on twitter.com"
fi
